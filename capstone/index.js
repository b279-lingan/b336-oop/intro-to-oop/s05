/*
CAPSTONE s05 - Objectives

Simulate the following basic e-commerce functionalities through the use of classes and objects:
customer registration
add product
update product price
archive product
add to cart
show cart
update cart quantity
clear cart
compute cart total
check out

Requirements

1. Customer class:
    email property - string
    cart property - instance of Cart class
    orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}
    checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty

2. Product class:
    name property - string
    price property - number
    isActive property - Boolean: defaults to true
    archive() method - will set isActive to false if it is true to begin with
    updatePrice() method - replaces product price with passed in numerical value

3. Cart class:
    contents property - array of objects with structure: {product: instance of Product class, quantity: number}
    totalAmount property - number
    addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property
    showCartContents() method - logs the contents property in the console
    updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.
    clearCartContents() method - empties the cart contents
    computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
*/

// =================================================================

class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive() {
        if (this.isActive) {
            this.isActive = false;
        }
    }

    updatePrice(newPrice) {
        this.price = newPrice;
    }
}

class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity) {
        this.contents.push({ product, quantity });
    }

    showCartContents() {
        this.contents.forEach(item => {
            console.log(`Product: ${item.product.name}, Quantity: ${item.quantity}`);
        });
    }

    updateProductQuantity(productName, newQuantity) {
        const item = this.contents.find(item => item.product.name === productName);
        if (item) {
            item.quantity = newQuantity;
        }
    }

    clearCartContents() {
        this.contents = [];
    }

    computeTotal() {
        this.totalAmount = this.contents.reduce((total, item) => {
            return total + item.product.price * item.quantity;
        }, 0);
    }
}

class Customer {
    constructor(email, cart, orders) {
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut() {
        if (this.cart.contents.length > 0) {
            this.cart.computeTotal();
            this.orders.push({ products: this.cart.contents, totalAmount: this.cart.totalAmount });
            this.cart.clearCartContents();
        }
    }
}

